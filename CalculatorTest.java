import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }

    @Test
    public void testAdd2(){
	double n1 = 7.2;
	double n2 = 5.7;
	double expected = 12.9;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testMultiply(){
	double n1 = 2.0;
	double n2 = 5.0;
	double expected = 10.0;
	double result = Calculator.multiply(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testDivide(){
	double n1 = 15.0;
	double n2 = 2.0;
	double expected = 7.5;
	double result = Calculator.divide(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

     public void testAbs(){
	double n1 = -23.8;
	double expected = 23.8;
	double result = Calculator.abs(n1);
	assertEquals(expected, result, 1e-6);
    }

     public void testPower(){
	double n1 = 2.0;
	int n2 = 6;
	double expected = 64.0;
	double result = Calculator.power(n1, n2);
	assertEquals(expected, result, 1e-6);
    }
}
